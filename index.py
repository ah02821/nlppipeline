from flask import Flask, request, jsonify
import torch
from transformers import AutoTokenizer, AutoModelForTokenClassification
import numpy as np
import os
import requests
import time
import concurrent.futures

# Set the model path to the local directory where the model is stored
model_path = "/user/HS401/ah02821/Downloads/distilbased"  # Ensure this is the correct path to your model directory

# Check if the model directory exists
if not os.path.isdir(model_path):
    raise FileNotFoundError(f"Model directory {model_path} not found. Please ensure the path is correct.")

# Load the saved model and tokenizer from the local directory
try:
    model = AutoModelForTokenClassification.from_pretrained(model_path)
    tokenizer = AutoTokenizer.from_pretrained(model_path)
except Exception as e:
    raise EnvironmentError(f"Error loading model from {model_path}: {e}")

# Define the label encoding (same as used during training)
label_encoding = {"B-O": 0, "B-AC": 1, "B-LF": 2, "I-LF": 3}
label_decoding = {v: k for k, v in label_encoding.items()}

# Function to predict labels for user input tokens
def predict_ner(tokens):
    try:
        # Tokenize the input tokens
        tokenized_input = tokenizer(tokens, is_split_into_words=True, return_tensors="pt", truncation=True)

        # Make predictions
        with torch.no_grad():
            output = model(**tokenized_input)

        predictions = np.argmax(output.logits.numpy(), axis=2)

        # Convert predictions to labels
        predicted_labels = [label_decoding[label] for label in predictions[0]]

        # Combine tokens and labels
        token_label_pairs = list(zip(tokens, predicted_labels))

        return token_label_pairs
    except Exception as e:
        print(f"Error during prediction: {e}")
        return None

# Initialize Flask app
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    data = request.json
    tokens = data.get("tokens")
    if not tokens:
        return jsonify({"error": "No tokens provided"}), 400

    predictions = predict_ner(tokens)
    if predictions is None:
        return jsonify({"error": "Prediction error"}), 500

    # Write predictions to the file
    with open("results.txt", "a") as file:
        file.write("Prediction:\n")
        for token, label in predictions:
            file.write(f"{token}: {label}\n")
        file.write("\n")

    return jsonify(predictions)

@app.route('/stress_test', methods=['POST'])
def stress_test():
    data = request.json
    tokens = data.get("tokens")
    if not tokens:
        return jsonify({"error": "No tokens provided"}), 400

    all_predictions = []
    with open("results.txt", "a") as file:
        file.write("Stress Test:\n")
        for i in range(50):
            predictions = predict_ner(tokens)
            if predictions is None:
                return jsonify({"error": "Prediction error during stress test"}), 500
            all_predictions.append(predictions)
            file.write(f"Iteration {i+1}:\n")
            for token, label in predictions:
                file.write(f"{token}: {label}\n")
            file.write("\n")

    return jsonify(all_predictions)

# Function to send a single request
def send_request(url, payload, headers):
    response = requests.post(url, json=payload, headers=headers)
    return response.status_code, response.elapsed.total_seconds()

# Function to run the stress test
@app.route('/run_stress_test', methods=['POST'])
def run_stress_test():
    data = request.json
    num_requests = data.get("num_requests", 2000)  # Default to 2000 requests if not provided
    url = "http://localhost:5003/predict"
    payload = {
        "tokens": ["For", "this", "purpose", "the", "Gothenburg", "Young", "Persons", "Empowerment", "Scale", "(", "GYPES", ")", "was", "developed", "."]
    }
    headers = {
        "Content-Type": "application/json"
    }

    times = []
    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = [executor.submit(send_request, url, payload, headers) for _ in range(num_requests)]
        for future in concurrent.futures.as_completed(futures):
            status_code, elapsed_time = future.result()
            times.append(elapsed_time)
            print(f"Status Code: {status_code}, Time: {elapsed_time} seconds")

    avg_time = sum(times) / len(times)
    max_time = max(times)
    min_time = min(times)

    with open("results.txt", "a") as file:
        file.write(f"Stress Test Results:\n")
        file.write(f"Average Response Time: {avg_time} seconds\n")
        file.write(f"Max Response Time: {max_time} seconds\n")
        file.write(f"Min Response Time: {min_time} seconds\n")
        file.write("\n")

    return jsonify({
        "average_response_time": avg_time,
        "max_response_time": max_time,
        "min_response_time": min_time
    })

@app.route('/get_latest_results', methods=['GET'])
def get_latest_results():
    try:
        with open("results.txt", "r") as file:
            lines = file.readlines()
            latest_results = lines[-10:]  # Get the last 10 lines or adjust as needed
        return jsonify({"latest_results": latest_results})
    except Exception as e:
        return jsonify({"error": f"Error reading results file: {e}"}), 500

@app.route('/clear_results', methods=['POST'])
def clear_results():
    try:
        open("results.txt", "w").close()
        return jsonify({"message": "Results cleared successfully."})
    except Exception as e:
        return jsonify({"error": f"Error clearing results file: {e}"}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003, debug=True)

